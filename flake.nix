{
  description = "canvas_integration packaged using poetry2nix";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    poetry2nix,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgName = "canvas_integration";
      pkgs = nixpkgs.legacyPackages.${system};
      inherit
        (poetry2nix.lib.mkPoetry2Nix {inherit pkgs;})
        mkPoetryApplication
        defaultPoetryOverrides
        ;
    in {
      packages = {
        ${pkgName} = mkPoetryApplication {
          projectDir = self;
          propagatedBuildInputs = [pkgs.pandoc];
          overrides = defaultPoetryOverrides.extend (self: super: {
            click-logging = super.click-logging.overridePythonAttrs (
              old: {
                buildInputs =
                  (old.buildInputs or [])
                  ++ [super.setuptools];
              }
            );
            canvasapi = super.canvasapi.overridePythonAttrs (
              old: {
                buildInputs =
                  (old.buildInputs or [])
                  ++ [super.setuptools];
              }
            );
          });
        };

        default = self.packages.${system}.${pkgName};
      };

      devShells.default = pkgs.mkShell {
        inputsFrom = [self.packages.${system}.${pkgName}];
        packages = with pkgs; [poetry commitizen pre-commit];
      };

      formatter = pkgs.alejandra;
    });
}
