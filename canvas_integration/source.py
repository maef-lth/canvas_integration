import enum
import logging
from copy import deepcopy
from pathlib import Path

import panflute as pf
from bs4 import BeautifulSoup

logger = logging.getLogger("canvas_integration." + __name__)


def definition_list_from_dict(metadata: dict) -> str:
    if len(metadata) == 0:
        return ""

    metadata_section = """
    <br />
    <hr />
    <section id="canvas_integration_metadata">
      <details>
        <summary>canvas_integration metadata</summary>
        <dl>
    """

    for key in metadata:
        metadata_section += f"""
            <dt>{key}</dt>
            <dd>{metadata[key]}</dd>
        """
    return metadata_section + "</dl></details></section>"


def prettify_html(html: str) -> str:
    return BeautifulSoup(html, "html.parser").prettify()


class CanvasTargetType(enum.Enum):
    FILE = enum.auto()
    PAGE = enum.auto()


class Source:
    SUFFIXES: list[str] = []
    INPUT_CONVERT_TEXT_KWARGS = {}
    OUTPUT_CONVERT_TEXT_KWARGS = {}
    METADATA_PREFIX_KEY = None
    UPLOAD_KWARGS = {}
    CANVAS_TARGET_TYPE: CanvasTargetType = CanvasTargetType.FILE

    def __init__(self, path):
        self.path: Path = Path(path)

        with self.path.open(mode="r") as fp:
            text = fp.read()
            self.doc = pf.convert_text(text, **self.INPUT_CONVERT_TEXT_KWARGS)

        self.original = deepcopy(self.doc)

        if not self.METADATA_PREFIX_KEY:
            self.metadata = self.doc.metadata
        else:
            self.metadata = self.doc.metadata[self.METADATA_PREFIX_KEY]

        if "canvas_integration" not in self.metadata:
            self.metadata["canvas_integration"] = {}

    def transform_for_upload(self, new_metadata: dict = None) -> str:
        """Transform the source file contents for upload to Canvas."""
        if new_metadata:
            for key in new_metadata:
                if not self.metadata["canvas_integration"].content.dict.get(key):
                    self.metadata["canvas_integration"][key] = new_metadata[key]

        convert_text_kwargs = deepcopy(self.OUTPUT_CONVERT_TEXT_KWARGS)

        if not convert_text_kwargs.get("extra_args"):
            convert_text_kwargs["extra_args"] = []

        convert_text_kwargs["extra_args"] += [
            # use work dir + parent dir for file for resource path
            f"--resource-path={self.path.parent}:."
        ]

        return pf.convert_text(self.doc, input_format="panflute", **convert_text_kwargs)

    def get_target_path(self, path_relative_to: Path | None = None):
        """Get the target path for the source file."""
        try:
            title = self.get_target_path_from_metadata()
        except (NotImplementedError, KeyError):
            title = self.get_target_path_from_path(path_relative_to=path_relative_to)

        return title

    def get_target_path_from_metadata(self):
        """Gets the target path for the source file from metadata."""
        raise NotImplementedError()

    def get_target_path_from_path(self, path_relative_to: Path | None = None):
        """Get the target path for the source file using its file path."""
        logger.debug(
            "Constructing target path from path with path_relative_to set to: "
            + str(path_relative_to)
        )
        if path_relative_to:
            path = self.path.relative_to(path_relative_to)
        else:
            path = self.path

        target_path = "-".join(path.parts)
        logger.debug(f"Constructed target path: {target_path}")

        return target_path


class MarkdownSource(Source):
    SUFFIXES = [".MD", ".MARKDOWN"]
    INPUT_CONVERT_TEXT_KWARGS = {
        "input_format": "markdown",
        "standalone": True,
        "extra_args": None,
    }
    OUTPUT_CONVERT_TEXT_KWARGS = {
        "output_format": "html",
        "standalone": False,
        "extra_args": None,
    }
    CANVAS_TARGET_TYPE: CanvasTargetType = CanvasTargetType.PAGE

    @staticmethod
    def panflute_action_set_title(elem, doc):
        if isinstance(elem, pf.Header):
            if elem.level == 1:
                if "title" not in doc.metadata:
                    doc.metadata["title"] = pf.stringify(elem)
                    # delete h1 if picked up as title
                    return []
                elif pf.stringify(elem) == doc.metadata["title"]:
                    # delete h1 if matching already set title
                    return []
                else:
                    # else demote all headers
                    doc.behead_all = True

            if doc.behead_all:
                elem.level += 1

                return elem

    def __init__(self, path):
        super().__init__(path)

        self.doc = pf.run_filters(
            [self.panflute_action_set_title],
            prepare=lambda doc: setattr(doc, "behead_all", False),
            finalize=lambda doc: delattr(doc, "behead_all"),
            doc=self.doc,
        )

    def transform_for_upload(self, new_metadata: dict = None) -> str:
        """Transform the source file contents for upload to Canvas."""
        transformed = super().transform_for_upload(new_metadata)

        transformed += definition_list_from_dict(
            # This only works for dictionaries without nesting..
            # On the other hand, stringify will preserve the data,
            # without breaks between items
            {
                key: pf.stringify(value)
                for key, value in self.metadata[
                    "canvas_integration"
                ].content.dict.items()
            }
        )
        return prettify_html(transformed)

    def get_target_path_from_metadata(self):
        """Gets the target path for the source file from metadata."""
        identifier = pf.stringify(self.metadata["title"])
        logging.debug(f"Found title in metadata: {identifier}")

        return identifier


class IPYNBSource(Source):
    SUFFIXES = [".IPYNB"]
    INPUT_CONVERT_TEXT_KWARGS = {
        "input_format": "ipynb",
        "standalone": True,
        "extra_args": None,
    }
    OUTPUT_CONVERT_TEXT_KWARGS = {
        "output_format": "ipynb",
        "standalone": True,
        "extra_args": None,
    }
    METADATA_PREFIX_KEY = "jupyter"

    def get_target_path_from_metadata(self):
        """Gets the target path for the source file from metadata."""
        identifier = pf.stringify(self.metadata["target_path"])
        logger.debug(f"Found identifier in metadata: {identifier}")

        return identifier


MAP_EXTENSION_SOURCE_CLASS = dict(
    [(suffix, MarkdownSource) for suffix in MarkdownSource.SUFFIXES]
    + [(suffix, IPYNBSource) for suffix in IPYNBSource.SUFFIXES]
)


def get_source_class_for_file(path: Path):
    try:
        class_ = MAP_EXTENSION_SOURCE_CLASS[path.suffix.upper()]
        logger.debug(f"Selected {class_} for {path}")
        return class_
    except KeyError:
        raise KeyError(f"The extension on the file {path.name} is not recognized.")
