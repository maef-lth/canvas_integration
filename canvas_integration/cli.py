import logging
import os
from pathlib import Path

import click
import click_logging
from dotenv import load_dotenv

from canvas_integration.canvas_course_client import CanvasCourseClient
from canvas_integration.upload import upload_files

logger = logging.getLogger("canvas_integration")
click_logging.basic_config(logger)


@click.group
@click_logging.simple_verbosity_option(logger)
@click.option("-e", "--env-file")
def cli(env_file):
    env_var_keys = [v for v in os.environ.keys() if v.startswith("CANVAS_")]
    logger.debug(f"CANVAS_ env vars before load_dotenv: {env_var_keys}")

    logger.debug(f"Loading .env at path: {env_file}. If None cwd is used.")
    load_dotenv(dotenv_path=env_file or None)

    env_var_keys = [v for v in os.environ.keys() if v.startswith("CANVAS_")]
    logger.debug(f"CANVAS_ env vars after load_dotenv: {env_var_keys}")


@cli.command
@click.argument("filepaths", nargs=-1, type=click.Path(exists=True))
@click.option(
    "-u",
    "--api-url",
    envvar="CANVAS_API_URL",
    required=True,
)
@click.option(
    "-c",
    "--course-code",
    type=int,
    envvar="CANVAS_COURSE_CODE",
    required=True,
)
@click.option(
    "-t",
    "--api-token",
    envvar="CANVAS_API_TOKEN",
    required=True,
)
@click.option(
    "-C", "--current-work-dir", help="Set a different working dir. Used for git lookups"
)
def upload(api_url, course_code, api_token, filepaths, current_work_dir=None):
    if current_work_dir:
        logging.debug(f"Current work dir given as argument: {current_work_dir}")
    else:
        current_work_dir = os.getcwd()
        logging.debug(f"Current work dir gotten from shell: {current_work_dir}")

    if len(filepaths) < 1:
        raise RuntimeError("No paths provided")

    filepaths = [Path(p).absolute() for p in filepaths]
    logger.debug(f"Input paths as pathlib paths: {filepaths}")

    paths_without_dirs = []

    while len(filepaths) > 0:
        path = filepaths.pop(0)

        if path.is_dir():
            filepaths += list(path.iterdir())
        else:
            paths_without_dirs.append(path.absolute())

    client = CanvasCourseClient(url=api_url, course_code=course_code, token=api_token)

    upload_files(
        paths_without_dirs,
        client,
        working_directory=Path(current_work_dir),
        ignore_unknown_file_types=len(paths_without_dirs) > 1,
    )


if __name__ == "__main__":
    cli()
