import logging
from pathlib import Path

from git.exc import InvalidGitRepositoryError
from pydriller import Git

from canvas_integration.canvas_course_client import CanvasCourseClient
from canvas_integration.source import (
    CanvasTargetType,
    Source,
    get_source_class_for_file,
)

logger = logging.getLogger("canvas_integration." + __name__)


def upload_files(
    file_paths: list[Path],
    client: CanvasCourseClient,
    working_directory: Path,
    ignore_unknown_file_types=True,
):
    try:
        git_repo = Git(working_directory)
        logger.info(f"Uploading from git repo: {git_repo.project_name}")
        logger.debug(f"Path to git repo: {git_repo.path}.")
    except InvalidGitRepositoryError:
        git_repo = None
        logger.debug(
            "No git repo at current work directory (or directory specified using -C)"
        )

    upload_return_values = []
    for path in file_paths:
        try:
            SourceClass = get_source_class_for_file(path)
        except KeyError:
            if ignore_unknown_file_types:
                logger.warning(
                    f"Skipping file with unrecognized file extension: {path}"
                )
                continue
            else:
                raise

        source = SourceClass(path)

        new_metadata = {}

        if git_repo:
            # absolute in case different work dir is specified
            file_specific_commits = git_repo.get_commits_modified_file(
                source.path.absolute()
            )

            last_commit = git_repo.get_commit(file_specific_commits[0])

            new_metadata["git_commit_hash"] = last_commit.hash
            new_metadata["git_commit_msg"] = last_commit.msg
            new_metadata["git_commit_author"] = last_commit.author.email
            new_metadata["git_commit_author_date"] = str(last_commit.author_date)

            logging.debug(
                f"Extracted following metadata from git commit: {new_metadata}"
            )

        ret_val = upload_source(
            client,
            source,
            new_metadata=new_metadata,
            working_directory=working_directory,
        )

        logger.debug(f"Return value from upload source: {ret_val}")
        upload_return_values.append(ret_val)
    return upload_return_values


def upload_source(
    client: CanvasCourseClient,
    source: Source,
    new_metadata: dict[str, str] | None = None,
    working_directory: Path | None = None,
):
    target_path = source.get_target_path(path_relative_to=working_directory)
    contents = source.transform_for_upload(new_metadata or {})

    logger.debug(
        f"Uploading source of type {source.CANVAS_TARGET_TYPE} to {target_path}."
    )

    if source.CANVAS_TARGET_TYPE not in CanvasTargetType:
        raise RuntimeError(
            f"CANVAS_TARGET_TYPE not recognized: {source.CANVAS_TARGET_TYPE}"
        )

    if source.CANVAS_TARGET_TYPE == CanvasTargetType.FILE:
        return client.upload_file(target_path, contents)

    if source.CANVAS_TARGET_TYPE == CanvasTargetType.PAGE:
        return client.create_page(target_path, contents, overwrite=True)
