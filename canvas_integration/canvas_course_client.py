import logging
import os
from pathlib import Path
from tempfile import TemporaryDirectory

from canvasapi import Canvas
from canvasapi.exceptions import Conflict
from canvasapi.folder import Folder as CanvasFolder

logger = logging.getLogger("canvas_integration." + __name__)


class CanvasCourseClient:
    def __init__(
        self,
        url: str | None = None,
        token: str | None = None,
        course_code: int | None = None,
    ):
        url = url or os.environ.get("CANVAS_API_URL")
        if url is None:
            raise RuntimeError(
                "API url needs to be set as an argument or as an environment "
                + "variable (CANVAS_API_URL)."
            )

        token = token or os.environ.get("CANVAS_API_TOKEN")
        if token is None:
            raise RuntimeError(
                "API token needs to be set as an argument or as an environment "
                + "variable (CANVAS_API_TOKEN)."
            )

        if course_code:
            course_code = int(course_code)
        elif "CANVAS_COURSE_CODE" in os.environ and os.environ.get(
            "CANVAS_COURSE_CODE"
        ):
            course_code = int(os.environ["CANVAS_COURSE_CODE"])
        else:
            raise RuntimeError(
                "Canvas course code needs to be set as an argument or as an "
                + "environment variable (CANVAS_COURSE_CODE)."
            )

        self.url: str = url
        self.token: str = token
        self.course_code: int = course_code

        self.api = Canvas(self.url, self.token)

        self.course = self.api.get_course(self.course_code)

    def get_folder(self, folder_name: str) -> CanvasFolder:
        try:
            return next(
                folder
                for folder in self.course.get_folders()
                if folder.full_name == f"course files/unfiled/{folder_name}"
            )
        except StopIteration:
            return self.course.create_folder(folder_name)

    def upload_file(
        self,
        target_file_name: str | None = None,
        contents: str | None = None,
        path: os.PathLike | None = None,
        target_folder: str = "canvas_integration",
    ):
        upload_target: CanvasFolder = self.get_folder(target_folder)

        if path:
            return upload_target.upload(Path(path))

        if target_file_name is None:
            raise RuntimeError(
                "Canvas file name needs to be given as target_file_name "
                + "parameter when not using existing file."
            )

        if contents is None:
            raise RuntimeError(
                "If path is not specified then contents needs to be set to a "
                + "string or byte string."
            )

        with TemporaryDirectory(prefix=__name__) as dir:
            canvas_file_path = Path(dir) / target_file_name

            open_kwargs = (
                {"mode": "w+", "encoding": "utf-8"}
                if isinstance(contents, str)
                else {"mode": "w+b"}
            )

            with canvas_file_path.open(**open_kwargs) as fp:
                fp.write(contents)
                logger.debug(f"Wrote file contents to {canvas_file_path}")

            ret = upload_target.upload(canvas_file_path)
            logger.debug(f"Return from upload_target.upload: {ret}")
            return ret

    def create_page(self, title, body, overwrite=False):
        wiki_page = {"title": title, "body": body}

        title_matches = self.course.get_pages(search_term=title)

        if len(list(title_matches)) > 0:
            if not overwrite:
                raise Conflict(
                    "Page or pages with matching titles already exists and "
                    + "overwrite is set to False."
                )

            # edit if exists
            return title_matches[0].edit(wiki_page=wiki_page)

        # create if not exists
        return self.course.create_page(wiki_page=wiki_page)
