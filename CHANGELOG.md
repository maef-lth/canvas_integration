# Changelog

All notable changes to this project will be documented in this file. Changelog
entries are created from commits (which should follow the [Conventional Commits
specification](https://www.conventionalcommits.org)).

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

## 0.3.1 (2024-01-24)

### Fix

- source

## 0.3.0 (2024-01-24)

### Feat

- **upload**: allow continue on unrecognized file, fix tests

## 0.2.1 (2024-01-17)

### Fix

- **cli**: fix names cli options

## 0.2.0 (2024-01-17)

### Feat

- **cli**: no prompting for url, course code or api token, but marked required, for non-interactive use

### Fix

- **client**: use unfiled/canvas_integration dir as default for files
- **tests**: fix mock target name (s/upload_function/upload_files/g)

## 0.1.5 (2023-03-24)

### Fix

- **source**: convert Path to str to be able to print it
- **deps**: python-dotenv now needed at runtime so moved from dev group

## 0.1.4 (2023-03-24)

### Fix

- **source**: fixed CANVAS_TARGET_TYPE for Markdown to be PAGE
- **tests**: fix mock target name (s/upload_function/upload_files/g)
- **upload,cli**: fixes and tests

## 0.1.3 (2023-02-09)

### Fix

- **upload.py**: fix get target_path: replace .get since it doesnt exist on metamap

## 0.1.2 (2023-02-02)

### Fix

- **upload**: stringify target_file_name if taken from pandoc metadata

## 0.1.1 (2023-02-02)

### Fix

- **upload.py**: use pf.stringify to get string of title and not pandoc MetaInline element

## 0.1.0 (2023-02-02)

### Feat

- **cli**: cli added, some refactoring, pypandoc replaced with panflute
