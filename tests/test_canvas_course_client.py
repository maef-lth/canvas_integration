import os
import pathlib
import re
import shutil
import uuid

import panflute as pf
import pytest
from bs4 import BeautifulSoup, Comment
from canvasapi.exceptions import Conflict, InvalidAccessToken, ResourceDoesNotExist
from dotenv import load_dotenv

from canvas_integration.canvas_course_client import CanvasCourseClient


@pytest.fixture
def env_values(TESTS_DIR):
    load_dotenv(TESTS_DIR / ".env")

    return {
        key: os.environ[key]
        for key in ("CANVAS_API_URL", "CANVAS_API_TOKEN", "CANVAS_COURSE_CODE")
    }


@pytest.fixture
def env_values_as_kwargs(env_values):
    return {
        "url": env_values["CANVAS_API_URL"],
        "token": env_values["CANVAS_API_TOKEN"],
        "course_code": env_values["CANVAS_COURSE_CODE"],
    }


@pytest.fixture
def html_file(FIXTURES_DIR):
    return FIXTURES_DIR / "canvas_ok_html.html"


@pytest.fixture
def html_div_soup(html_file):
    with html_file.open(mode="r") as fp:
        return BeautifulSoup(fp.read(), "html.parser")


@pytest.fixture
def html_p_soup():
    return BeautifulSoup(
        "<p>simple <b>html</b> string<!-- with a comment --></p>", "html.parser"
    )


class TestCanvas:
    @pytest.fixture
    def client(self, env_values_as_kwargs):
        return CanvasCourseClient(**env_values_as_kwargs)

    # add this again if there's problems
    # Let's trust canvas lms api rate limiting for now
    # def teardown_method(self):
    #     """Sleep between API auths"""
    #     time.sleep(1)


class TestCanvasCourseClientSetup(TestCanvas):
    def test_setup_using_environment(self, monkeypatch, env_values):
        for key in env_values:
            monkeypatch.setenv(key, env_values[key])

        client = CanvasCourseClient()

        client.course.get_folders()

    def test_setup_using_wrong_environment(self, monkeypatch, env_values):
        for key in env_values:
            value = env_values[key]

            if key == "CANVAS_API_TOKEN":
                value = value[::-1]  # reversed

            monkeypatch.setenv(key, value)

        with pytest.raises(InvalidAccessToken):
            CanvasCourseClient()

    def test_setup_using_kwargs(self, env_values_as_kwargs):
        client = CanvasCourseClient(**env_values_as_kwargs)

        client.course.get_folders()

    def test_setup_wrong_kwargs(self, env_values_as_kwargs):
        env_values_as_kwargs["course_code"] = 1

        with pytest.raises(ResourceDoesNotExist):
            CanvasCourseClient(**env_values_as_kwargs)


def create_unique_id() -> str:
    return str(uuid.uuid4()).split("-")[0]


def create_unique_path_stem(path: os.PathLike) -> str:

    path = pathlib.Path(path)

    stem = path.stem

    return stem + create_unique_id()


class TestCanvasCourseClient(TestCanvas):
    def test_get_folder(self, client):
        # random folder name
        folder_name = f"canvas_integration_test_get_folder_{create_unique_id()}"
        folder_full_name = f"course files/unfiled/{folder_name}"

        folder_first_try = client.get_folder(folder_name)
        folder_second_try = client.get_folder(folder_name)

        assert folder_first_try.full_name == folder_full_name
        assert folder_second_try.full_name == folder_full_name

        folder_first_try.delete()
        # don't know why this works twice
        folder_second_try.delete()

    @pytest.mark.parametrize("folder", ["canvas_integration_test", None])
    @pytest.mark.parametrize(
        "kwargs",
        [
            {
                "target_file_name": "canvas_integration_upload_bytes_file.test",
                "contents": "test".encode(),
            },
            {
                "target_file_name": "canvas_integration_upload_text_file.test",
                "contents": "test",
            },
            {
                "target_file_name": "canvas_integration_upload_text_file2.test",
                "path_fixture": "ipynb_test_file_1_path",
            },
        ],
    )
    def test_upload_bytes_file(self, client, folder, kwargs, request, tmp_path):
        kwargs["target_folder"] = folder
        orig_path: pathlib.Path | None = None

        if kwargs.get("path_fixture"):
            kwargs["path"] = request.getfixturevalue(kwargs["path_fixture"])
            del kwargs["path_fixture"]

        if kwargs.get("path"):
            orig_path = src = kwargs["path"]

            dst = tmp_path / create_unique_path_stem(src)

            shutil.copy(src, dst)

            kwargs["path"] = dst

        if kwargs.get("target_file_name"):
            kwargs["target_file_name"] = create_unique_path_stem(
                kwargs["target_file_name"]
            )

        ok, json_resp = client.upload_file(**kwargs)

        assert ok
        assert json_resp["upload_status"] == "success"

        file_id = json_resp["id"]
        download = client.course.get_file(file_id)

        downloaded_contents = download.get_contents()

        if kwargs.get("contents"):
            assert (
                downloaded_contents == kwargs["contents"]
                or downloaded_contents.encode() == kwargs["contents"]
            )
        else:
            with orig_path.open(mode="r") as fp:
                assert downloaded_contents == fp.read()

        download.delete()

    @staticmethod
    def assert_equal_html(first, second):
        """Assert first and second are equal, given Canvas LMS modifications."""
        postprocessed = []
        for html in (first, second):
            html = BeautifulSoup(str(html), "html.parser")

            # canvas removes all comments
            for comm in html.find_all(string=lambda text: isinstance(text, Comment)):
                comm.extract()

            # canvas adds a class and a few data attributes to embed & object
            for embedlike in html.find_all(["embed", "object"]):
                del embedlike["class"]
                data_keys = [key for key in embedlike.attrs if key.startswith("data-")]
                for key in data_keys:
                    del embedlike[key]

            # remove BOM and whitespace to fix whitespace differences
            # pandoc leaves </hr> at end of document sometimes
            # canvas adds its own url to relative href's
            # canvas adds tbody (or pandoc removes?)
            stripped = re.sub(
                r"(\s|\ufeff|</hr>|</?tbody>|https?://(canvas\.education\.lu\.se)?)",
                "",
                str(html),
            )

            # convert <hr/> to <hr>, <br/> to <br> etc
            normalized = re.sub(r"<([a-zA-Z]+)/>", r"<\1>", stripped)

            postprocessed.append(normalized)

        first, second = postprocessed
        assert first == second

    @pytest.mark.parametrize("fixture_body_soup_name", ["html_p_soup", "html_div_soup"])
    def test_create_page_create_no_overwrite(
        self, request, client, fixture_body_soup_name
    ):
        title = "test create page no overwrite"
        fixture_body_soup = request.getfixturevalue(fixture_body_soup_name)

        title_matches = client.course.get_pages(search_term=title)

        for p in title_matches:
            p.delete()

        page = client.create_page(title, fixture_body_soup.prettify())

        self.assert_equal_html(fixture_body_soup, page.body)

        assert page.title == title

        with pytest.raises(Conflict):
            client.create_page(title, fixture_body_soup.prettify())

    @pytest.mark.parametrize("fixture_body_soup_name", ["html_p_soup", "html_div_soup"])
    @pytest.mark.parametrize("run_number", [1, 2])  # just to test overwriting
    def test_create_page_create_overwrite(
        self, request, client, fixture_body_soup_name, run_number
    ):
        title = "test create page overwrite"
        fixture_body_soup = request.getfixturevalue(fixture_body_soup_name)

        new_tag = fixture_body_soup.new_tag("p")
        new_tag.string = f"upload number {run_number}"

        fixture_body_soup.append(new_tag)

        page = client.create_page(title, fixture_body_soup.prettify(), overwrite=True)

        self.assert_equal_html(fixture_body_soup, page.body)

        assert page.title == title

    def test_create_page_from_markdown_source(self, client, markdown_source):
        body = markdown_source.transform_for_upload()

        title = pf.stringify(markdown_source.metadata["title"])

        page = client.create_page(title, body, overwrite=True)

        self.assert_equal_html(body, page.body)
        assert page.title == title
