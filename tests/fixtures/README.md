# Fixtures

## `file.md`

[Original file copied from pandoc's
repo](https://github.com/jgm/pandoc/blob/main/test/writer.markdown).
[Pandoc](https://pandoc.org) is [Copyright (C) 2006-2023 John MacFarlane <jgm at
berkeley dot edu> and released under a modified version of
GPL2](https://github.com/jgm/pandoc/blob/main/COPYRIGHT).

Additions to front matter.

## `file.ipynb`

[Original file copied from pandoc's
repo](https://github.com/jgm/pandoc/blob/main/test/ipynb/simple.ipynb).
[Pandoc](https://pandoc.org) is [Copyright (C) 2006-2023 John MacFarlane <jgm at
berkeley dot edu> and released under a modified version of
GPL2](https://github.com/jgm/pandoc/blob/main/COPYRIGHT).

Metadata added.
