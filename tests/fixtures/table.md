---
canvas_integration:
  target_path: test
---
# Document Title

|       | Column 1 | Column 2 | Column 3 |
|-------|----------|----------|----------|
| Row 1 | Cell 1   | Cell 2   | Cell 3   |
| Row 2 | Cell 4   | Cell 5   | Cell 6   |
| Row 3 | Cell 7   | Cell 8   | Cell 9   |
| Row 4 | Cell 10  | Cell 11  | Cell 12  |
