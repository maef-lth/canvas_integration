import pathlib
import tempfile

import pytest

from canvas_integration.source import IPYNBSource, MarkdownSource


def create_temp_file_copy(path: pathlib.Path):
    with path.open(mode="r") as orig_fp:
        text = orig_fp.read()

    with tempfile.NamedTemporaryFile(
        mode="w", suffix=path.suffix, delete=False
    ) as copy_fp:
        copy_fp.write(text)

        return pathlib.Path(copy_fp.name)


@pytest.fixture
def TESTS_DIR():
    return pathlib.Path(__file__).parent


@pytest.fixture
def REPO_DIR(TESTS_DIR):
    return TESTS_DIR.parent


@pytest.fixture
def FIXTURES_DIR(TESTS_DIR):
    return TESTS_DIR / "fixtures"


@pytest.fixture
def markdown_test_file_1_path(FIXTURES_DIR):
    return FIXTURES_DIR / "file.md"


@pytest.fixture
def markdown_test_file_1_path_copy(markdown_test_file_1_path):
    return create_temp_file_copy(markdown_test_file_1_path)


@pytest.fixture
def markdown_source(markdown_test_file_1_path):
    return MarkdownSource(markdown_test_file_1_path)


@pytest.fixture
def ipynb_test_file_1_path(FIXTURES_DIR):
    return FIXTURES_DIR / "file.ipynb"


@pytest.fixture
def ipynb_test_file_1_path_copy(ipynb_test_file_1_path):
    return create_temp_file_copy(ipynb_test_file_1_path)


@pytest.fixture
def ipynb_test_file_2_path(FIXTURES_DIR):
    return FIXTURES_DIR / "file_wo_img.ipynb"


@pytest.fixture
def ipynb_test_file_2_path_copy(ipynb_test_file_2_path):
    return create_temp_file_copy(ipynb_test_file_2_path)


@pytest.fixture
def ipynb_source(ipynb_test_file_1_path):
    return IPYNBSource(ipynb_test_file_1_path)
