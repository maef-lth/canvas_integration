import panflute as pf
import pytest
from bs4 import BeautifulSoup

from canvas_integration.source import prettify_html


@pytest.fixture
def non_pretty_html():
    return """
        <h1>Copying Beautiful Soup objects<a
        href="#copying-beautiful-soup-objects">¶</a></h1><p>You can use <code
        class="docutils literal notranslate"> <span
        class="pre">copy.copy()</span></code> to create a copy of any <code
        class="docutils literal notranslate"><span class="pre">Tag</span></code>
        or<code class="docutils literal notranslate"><span
        class="pre">NavigableString</span></code>:</p><div
        class="highlight-default notranslate"><div
        class="highlight"><pre><span></span><span class="kn">import</span> <span
        class="nn">copy</span><span class="n">p_copy</span> <span
        class="o">=</span> <span class="n">copy</span><span
        class="o">.</span><span class="n">copy</span><span
        class="p">(</span><span class="n">soup</span><span
        class="o">.</span><span class="n">p</span><span class="p">)</span><span
        class="nb">print</span> <span class="n">p_copy</span><span class="c1">#
        &lt;p&gt;I want &lt;b&gt;pizza&lt;/b&gt; and more
        &lt;b&gt;pizza&lt;/b&gt;!&lt;/p&gt;
    """


def test_prettify_html(non_pretty_html):
    assert (
        prettify_html(non_pretty_html)
        == BeautifulSoup(non_pretty_html, "html.parser").prettify()
    )


@pytest.mark.parametrize("src_fixture", ["markdown_source", "ipynb_source"])
class TestSource:
    @pytest.fixture
    def src(self, request, src_fixture):
        return request.getfixturevalue(src_fixture)

    def test_create_source(self, src):
        assert src.doc

    def test_load_data(self, src):
        assert len(src.doc.content) > 0
        assert isinstance(src.doc.metadata, pf.MetaMap)

    def test_insert_into_metadata(self, src):
        src.metadata["canvas_integration"]["test"] = 5

        # fix for metadata all under "jupyter" key on ipynb
        key_str = "canvas_integration.test"
        if src.METADATA_PREFIX_KEY:
            key_str = src.METADATA_PREFIX_KEY + "." + key_str
        assert src.doc.get_metadata(key_str) == str(5)

    def test_transform_for_upload_naive(self, src):
        _ = src.transform_for_upload()


class TestMarkdownSource:
    def test_title(self, markdown_source):
        assert (
            pf.stringify(markdown_source.doc.metadata["title"]) == "Pandoc Test Suite"
        )

    def test_transform_for_upload(self, markdown_source):
        output = markdown_source.transform_for_upload()
        soup = BeautifulSoup(output, "html.parser")

        assert soup.find("h2").get_text().strip() == "Headers"
        assert soup.find("h1") is None

        assert soup.find("table")

        metadata_section = soup.find(id="canvas_integration_metadata")
        dl = metadata_section.find("dl")

        assert "another_attr" in dl.text


class TestIPYNBSource:
    pass
