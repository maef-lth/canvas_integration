import logging
import tempfile
from pathlib import Path
from unittest.mock import ANY, MagicMock, patch

import pytest
from git.exc import InvalidGitRepositoryError

from canvas_integration.canvas_course_client import CanvasCourseClient
from canvas_integration.upload import upload_files

logger = logging.getLogger("canvas_integration." + __name__)

PARAMETRIZE_ARG_FILE_PATH_FIXTURES = (
    ["markdown_test_file_1_path"],
    ["ipynb_test_file_2_path"],
    ["markdown_test_file_1_path_copy", "markdown_test_file_1_path"],
    ["ipynb_test_file_2_path_copy", "markdown_test_file_1_path"],
)


@pytest.mark.parametrize(
    "file_path_fixtures",
    PARAMETRIZE_ARG_FILE_PATH_FIXTURES,
)
@patch("canvas_integration.upload.upload_source")
def test_upload_files(mock_upload_source, file_path_fixtures, request):
    client = MagicMock(spec=CanvasCourseClient)
    mock_upload_source.return_value = "upload_source called"

    file_paths = [request.getfixturevalue(f) for f in file_path_fixtures]

    working_directory = file_paths[0].parent

    result = upload_files(file_paths, client, working_directory)

    assert result == [mock_upload_source.return_value] * len(file_path_fixtures)

    # only tests last call
    mock_upload_source.assert_called_with(
        client, ANY, new_metadata=ANY, working_directory=working_directory
    )

    for file_path, call_args in zip(file_paths, mock_upload_source.call_arg_list):
        source_used = call_args.args[1]
        assert file_path == source_used.path


@pytest.mark.parametrize("suffix", [".txt", ".docx"])
def test_upload_files_raises_error_for_unrecognized_extension(suffix, TESTS_DIR):
    client = MagicMock(spec=CanvasCourseClient)
    working_directory = TESTS_DIR

    with tempfile.NamedTemporaryFile(mode="r", suffix=suffix) as fp:
        filepaths = [Path(fp.name)]

        # not ignore_unknown_file_types
        with pytest.raises(KeyError):
            upload_files(
                filepaths, client, working_directory, ignore_unknown_file_types=False
            )

        # ignore_unknown_file_types
        upload_files(
            filepaths, client, working_directory, ignore_unknown_file_types=True
        )


@pytest.mark.parametrize(
    "file_path_fixtures",
    PARAMETRIZE_ARG_FILE_PATH_FIXTURES,
)
@patch("canvas_integration.upload.upload_source")
@patch("canvas_integration.upload.Git")
def test_upload_with_git_repo(
    mock_git, mock_upload_source, file_path_fixtures, REPO_DIR, request
):
    client = MagicMock(spec=CanvasCourseClient)
    file_paths = [request.getfixturevalue(f) for f in file_path_fixtures]

    working_directory = REPO_DIR
    mock_git.return_value.get_commits_modified_file.return_value = ["12345"]
    mock_git.return_value.get_commit.return_value.hash = "12345"
    mock_git.return_value.get_commit.return_value.msg = "commit message"
    mock_git.return_value.get_commit.return_value.author.email = "author@example.com"
    mock_git.return_value.get_commit.return_value.author_date = "2022-03-22 12:00:00"

    upload_files(file_paths, client, working_directory)

    mock_upload_source.assert_called_with(
        client,
        ANY,
        new_metadata={
            "git_commit_hash": "12345",
            "git_commit_msg": "commit message",
            "git_commit_author": "author@example.com",
            "git_commit_author_date": "2022-03-22 12:00:00",
        },
        working_directory=working_directory,
    )


@pytest.mark.parametrize(
    "file_path_fixtures",
    PARAMETRIZE_ARG_FILE_PATH_FIXTURES,
)
@patch("canvas_integration.upload.Git", side_effect=InvalidGitRepositoryError)
@patch("canvas_integration.upload.upload_source")
def test_upload_without_git_repo(
    mock_upload_source, mock_git, file_path_fixtures, TESTS_DIR, request
):
    client = MagicMock(spec=CanvasCourseClient)
    mock_upload_source.return_value = "upload_source called"
    file_paths = [request.getfixturevalue(f) for f in file_path_fixtures]

    working_directory = TESTS_DIR
    result = upload_files(file_paths, client, working_directory)
    assert result == [mock_upload_source.return_value] * len(file_path_fixtures)
