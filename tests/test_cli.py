import os
import tempfile
from pathlib import Path
from unittest import mock

import click.testing
import pytest

from canvas_integration import cli


@pytest.fixture
def runner():
    return click.testing.CliRunner()


def test_cli(runner):
    result = runner.invoke(cli.cli, ["--help"])
    assert result.exit_code == 0
    assert "Usage: cli [OPTIONS] COMMAND [ARGS]..." in result.output


def test_upload_command_no_arguments(runner):
    result = runner.invoke(cli.upload)
    assert result.exit_code != 0
    assert isinstance(result.exception, RuntimeError)

    assert result.exc_info[1].args[0] == "No paths provided"


@mock.patch("canvas_integration.cli.CanvasCourseClient")
@mock.patch("canvas_integration.cli.upload_files")
def test_upload_command(mock_upload_function, mock_CanvasCourseClient, runner):
    mock_CanvasCourseClient._get_canvas_api = lambda _: None
    mock_CanvasCourseClient._get_course_api = lambda _: None

    with tempfile.TemporaryDirectory() as tmpdir:
        temp_file = Path(tmpdir) / "test.txt"
        temp_file.write_text("Test content")

        os.environ["CANVAS_API_URL"] = "https://canvas.example.com"
        os.environ["CANVAS_COURSE_CODE"] = "12345"
        os.environ["CANVAS_API_TOKEN"] = "fake_token"

        result = runner.invoke(
            cli.upload,
            [str(temp_file)],
        )

        assert result.exit_code == 0

        # Ensure that the CanvasCourseClient is instantiated with the correct parameters
        mock_CanvasCourseClient.assert_called_once_with(
            url="https://canvas.example.com",
            course_code=12345,
            token="fake_token",
        )
        # Ensure that the upload_function is called with the correct parameters
        mock_upload_function.assert_called_once_with(
            [temp_file],
            mock_CanvasCourseClient.return_value,
            working_directory=Path.cwd(),
            ignore_unknown_file_types=False,
        )


@mock.patch("canvas_integration.cli.CanvasCourseClient")
@mock.patch("canvas_integration.cli.upload_files")
def test_upload_command_nested_directory_structure(
    mock_upload_function, mock_canvas_course_client, runner
):
    with tempfile.TemporaryDirectory() as tmpdir:
        base_dir = Path(tmpdir)
        subdir = base_dir / "subdir"
        subdir.mkdir()
        temp_file1 = base_dir / "test1.txt"
        temp_file2 = subdir / "test2.txt"
        temp_file1.write_text("Test content 1")
        temp_file2.write_text("Test content 2")

        os.environ["CANVAS_API_URL"] = "https://canvas.example.com"
        os.environ["CANVAS_COURSE_CODE"] = "12345"
        os.environ["CANVAS_API_TOKEN"] = "fake_token"

        result = runner.invoke(
            cli.upload,
            [str(base_dir)],
        )

        assert result.exit_code == 0

        # Ensure that the upload_function is called with the correct parameters
        mock_upload_function.assert_called_once_with(
            [temp_file1, temp_file2],
            mock_canvas_course_client.return_value,
            working_directory=Path.cwd(),
            ignore_unknown_file_types=True,
        )
